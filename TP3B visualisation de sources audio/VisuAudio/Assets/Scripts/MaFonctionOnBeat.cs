using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaFonctionOnBeat : MonoBehaviour
{
    // Déclaration d'un délégué appelé OnBeatEventHandler. Il ne retourne rien et ne prend pas de paramètres.
    public delegate void OnBeatEventHandler();
    
    // Événement basé sur le délégué précédemment défini. Il peut être souscrit par d'autres méthodes.
    public OnBeatEventHandler onBeat;
    
    // Temps en secondes entre chaque "clignotement" ou battement.
    public float blinkTime = 1f;

    // Timer utilisé pour compter le temps écoulé depuis le dernier battement.
    private float timer;

    // Référence au Renderer de l'objet, utilisé pour activer/désactiver l'affichage.
    private Renderer rend;

    void Start()
    {
        // Souscrire la méthode HandleBeat à l'événement onBeat.
        onBeat += HandleBeat;

        // Récupérer le composant Renderer de cet objet.
        rend = GetComponent<Renderer>();

        // Initialiser le timer avec la valeur de blinkTime.
        timer = blinkTime;
    }

    void Update()
    {  
        // Si quelqu'un a souscrit à l'événement onBeat, invoque cet événement.
        onBeat?.Invoke();
    }

    void HandleBeat()
    {
        // Diminuer le timer selon le temps écoulé depuis la dernière frame.
        timer -= Time.deltaTime;

        // Si le timer atteint ou dépasse zéro...
        if (timer <= 0f)
        {
            // Inverser l'état d'affichage du Renderer (si actif, le désactiver; si inactif, l'activer).
            rend.enabled = !rend.enabled;

            // Réinitialiser le timer à la valeur de blinkTime.
            timer = blinkTime;
        }
    }
}
