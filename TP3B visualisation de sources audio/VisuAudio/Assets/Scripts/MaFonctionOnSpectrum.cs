using UnityEngine;
using System.Collections.Generic;

//même chose que MaFonctionOnBeat mais plutot que de faire clignotter un objet en activant et desactivant son renderer, 
//on va faire grossir et rétrécir des cubes en fonction de la musique
//A la maniere des barres de son d'une chaine hifi ou autres


public class MaFonctionOnSpectrum : MonoBehaviour
{
    public delegate void OnSpectrumEventHandler(float[] spectrum);
    public OnSpectrumEventHandler onSpectrum;

    public int barNombre = 64;
    public GameObject barPrefab; // Assignez un prefab de cube dans l'éditeur Unity
    public Vector3 barPositionInitial = new Vector3(-32, 0, 0); // Position de départ pour les barres

    private List<GameObject> bars = new List<GameObject>();

    void Start()
    {
        onSpectrum += HandleSpectrum;

        for (int i = 0; i < barNombre; i++)
        {
            GameObject bar = Instantiate(barPrefab, barPositionInitial + new Vector3(i * 2, 0, 0), Quaternion.identity);
            bars.Add(bar);
        }
    }

    void Update()
    {
        float[] spectrum = new float[barNombre];
        AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
        
        onSpectrum?.Invoke(spectrum);
    }

    void HandleSpectrum(float[] spectrum)
    {
        for (int i = 0; i < bars.Count; i++)
        {
            Vector3 previousScale = bars[i].transform.localScale;
            previousScale.y = Mathf.Lerp(previousScale.y, 1 + spectrum[i] * 40, Time.deltaTime * 10);
            bars[i].transform.localScale = previousScale;
        }
    }
}
