using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbite : MonoBehaviour
{
    public GameObject cible; // Cible autour de laquelle la caméra tournera.
    public float anglesParSeconde = 45; // Vitesse de rotation de la caméra.

    // Start is called before the first frame update
    void Start()
    {
        // Initialise la position et la rotation de la caméra.
        this.transform.position = new Vector3(2, 0, 0);
        this.transform.rotation = Quaternion.Euler(0, -90f, 0); 
    }

    void Update()
    {
        float rotationFactor = 0; // Initialisation à 0 pour chaque frame.

        // Si on appuie sur la flèche gauche.
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rotationFactor = -1; // Négatif pour tourner à gauche.
        }
        // Sinon, si on appuie sur la flèche droite.
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            rotationFactor = 1; // Positif pour tourner à droite.
        }

        // Ajoute la rotation basée sur la souris.
        rotationFactor += Input.GetAxis("Mouse X");

        // Si le facteur de rotation est différent de 0 (flèche directionnelle ou mouvement de la souris),
        // fait tourner la caméra.
        if (rotationFactor != 0)
        {
            transform.RotateAround(cible.transform.position, Vector3.up, anglesParSeconde * Time.deltaTime * rotationFactor);
        }
    }
}
