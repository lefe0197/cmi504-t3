using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script pour visualiser les données sur le globe


public class VisuData : MonoBehaviour
{
    public Transform Cible;
    public GameObject VisuCube;
    public float multiplier;
    public Color lowPopulationColor = Color.white; // Couleur pour une faible population
    public Color highPopulationColor = Color.red; // Couleur pour une population élevée

    Vector3 latloncart(float lat, float lon)
    {
        Vector3 pos; // Position du point sur la sphère
        pos.x = 0.5f * Mathf.Cos((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad); // Conversion degrés -> radians
        pos.y = 0.5f * Mathf.Sin(lat * Mathf.Deg2Rad); // Conversion degrés -> radians
        pos.z = 0.5f * Mathf.Sin((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad); // Conversion degrés -> radians
        return pos;
    }

    Color GetPopulationColor(int population)
    {
        int maxPopulation = 39105000; // Population maximale (Chine)
        int minPopulation = 0; // Population minimale (Antarctique)
        float t = Mathf.InverseLerp(minPopulation, maxPopulation, population); // Interpolation linéaire
        return Color.Lerp(lowPopulationColor, highPopulationColor, t); // Interpolation de la couleur
    }

    public void VisualCube(float lat, float lon, float val, float multiplier)
    {
        GameObject cube = GameObject.Instantiate(VisuCube);// Créez un cube
        Vector3 pos = latloncart(lat, lon); // Position du point sur la sphère
        cube.transform.position = pos; // Position du cube
        cube.transform.LookAt(Cible, Vector3.back); // Orientez le cube vers la caméra
        Vector3 echelle = cube.transform.localScale;// Echelle du cube
        echelle.z = val * multiplier; // Echelle du cube
        cube.transform.localScale = echelle; // Echelle du cube
        
        
        Renderer rend = cube.transform.Find("Cube").GetComponent<Renderer>();// Accédez à l'enfant "Cube" pour obtenir le Renderer
        rend.material.color = GetPopulationColor((int)val); // Couleur du cube
    }

}
